package br.unicamp.ic.inf300.sort;

public class VectorSorter {
	
	public static void main(String[] args) {
	
		int[] numbers = parseParameters(args);
	
		System.out.print("Input: ");
		printVector(numbers);
		sort(numbers);
		System.out.print("Sorted: ");
		printVector(numbers);
	}
	
	public static void sort(int[] numbers) {
		BubbleSort.sort(numbers);
	}
	
	public static int[] parseParameters(String[] args) {
		int[] numbers;
		if(args.length > 0) {
			numbers = new int[args.length];
			for(int k=0; k<args.length; k++) {
				numbers[k] = Integer.parseInt(args[k]);
			}
		}else {
			numbers = generateRandomVector(10);
		}
		return numbers;
	}
	
	private static int[] generateRandomVector(int size) {

		int[] vector = new int[size];

		for (int i = 2; i < vector.length ; i++) {
			vector[i] = (int) (Math.random()*100 + 1);
		}
		
		return vector;
	}
	
	public static void printVector(int[] numbers) {
		System.out.print("[ ");
		System.out.print(numbers[0]);
		
		int i = 1;
		do {
			i++;
			System.out.print(", ");
			System.out.print(numbers[i]);
		}while(i < numbers.length - 1);

		System.out.println(" ]");
	}
}
